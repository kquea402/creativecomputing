# Resources
### Learning
+ [Web Field Manual](https://webfieldmanual.com/design.html)
+ [Eloquent Javascript](http://eloquentjavascript.net/)

### Graphics
+ [p5](https://p5js.org/)
+ [Textures.js](https://riccardoscalco.it/textures/)

### Color
+ [Scale](https://hihayk.github.io/scale/)
+ [Coolors](https://coolors.co/)

### Type
+ [Colorfont.js](http://manufacturaindependente.com/colorfont/)
+ [Lettering.js](http://letteringjs.com/)
+ [Kerning.js](http://kerningjs.com/)
+ [Fittext.js](http://fittextjs.com/)
+ [Font Combinations](https://www.canva.com/font-combinations/)

### Typefaces
+ [Font Library](https://fontlibrary.org/en)
+ [Google Fonts](https://fonts.google.com/)

### Grid
+ [Simple Grid](http://simplegrid.io/)
+ [Masonry](https://masonry.desandro.com/)

### Framework
+ [Skeleton](http://getskeleton.com/)
+ [Milligram](https://milligram.io/)
+ [Bootstrap](https://getbootstrap.com/)

### CSS
+ [CSS Reset](https://meyerweb.com/eric/tools/css/reset/)

### 360 Photo/Video
+ [A-Frame](https://aframe.io/)
